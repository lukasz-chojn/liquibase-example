# Liquibase Example Application

###### An example application that shows how to configure the database versioning mechanism - Liquibase.
###### On startup, the application creates a database in a text file using the H2 database driver. Then it creates a base structure using Liquibase.
###### The entire configuration of the application is located in the application.yml file.
###### The pom.xml file contains the configuration of Liquibase plugin for maven.