package liquibase;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

@SpringBootTest
public class ApplicationTests {

    private static final String QUERY_COUNT_TAGS = "select count(ID) from CHANGELOG_DB";

    private static Connection connection;

    @BeforeClass
    public static void init() throws Exception {
        Class.forName("org.h2.Driver");
        connection = DriverManager.getConnection("jdbc:h2:file:./src/main/resources/db/testDB", "sa", "");
    }

    @Test
    public void shouldRunWithoutExceptions() throws SQLException {
        ResultSet resultSet = connection.createStatement()
                .executeQuery(QUERY_COUNT_TAGS);
        resultSet.next();
        Assert.assertEquals(3, resultSet.getInt(1));
    }
}
